package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import jp.alhinc.springtraining.entity.User;

@Mapper
public interface UserMapper {

	List<User> findAll();

	int create(User entity);

	User getUser(@Param(value="id")Long id);

	int update(User entity);

}
