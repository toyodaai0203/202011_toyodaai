package jp.alhinc.springtraining.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.Position;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.service.BranchService;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.EditUserService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.PositionService;

@SpringBootApplication
@ComponentScan("com.mongotest") //to scan packages mentioned
@EntityScan("com.example.entities")
//@EnableJpaRepositories("com.example.repositories")

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private EditUserService editUserService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private PositionService positionService;

//	@GetMapping
//	public String index(Model model) {
//		List<User> users = getAllUsersService.getAllUsers();
//		model.addAttribute("users", users);
//		System.out.println(users);
//		return "users/index";
//	}
//
//	@GetMapping("/create")
//	public String create(Model model) {
//		model.addAttribute("form", new CreateUserForm());
//		return "users/create";
//	}
//
//	@PostMapping
//	public String create(@Validated CreateUserForm form, BindingResult result, Model model) {
//
//		if (result.hasErrors()) {
//			model.addAttribute("message", "残念");
//			return "users/create";
//		}
//
//		createUserService.create(form);
//		return "redirect:/users";
//	}
//
//}


@GetMapping
public String index(Model model) {
	List<User> users = getAllUsersService.getAllUsers();
	model.addAttribute("users", users);
	System.out.println(users);
	return "users/index";
}

@GetMapping("/create")
public String create(Model model) {
	List<Branch> branches = branchService.getBranch();
	List<Position> positions = positionService.getPosition();
	model.addAttribute("form", new CreateUserForm());
	model.addAttribute("branches", branches);
	model.addAttribute("positions", positions);
	return "users/create";
}

@GetMapping("/edit")
public String update(@RequestParam Long id, Model model) {
	List<Branch> branches = branchService.getBranch();
	List<Position> positions = positionService.getPosition();
	model.addAttribute("editUserForm", new EditUserForm());
	model.addAttribute("branches", branches);
	model.addAttribute("positions", positions);
	return "users/edit";
}

@PostMapping
public String create(@Validated CreateUserForm form, BindingResult result, Model model) {

	if (result.hasErrors()) {
		model.addAttribute("message", "残念");
		return "redirect:/users/create";
	}

	createUserService.create(form);
	return "redirect:/users";
}

@PostMapping
public String update(@ModelAttribute("editUserForm") EditUserForm form, BindingResult result, Model model) {
	editUserService.update(form);
	model.addAttribute("message", "変更が完了しました");
	return "users/edit";
}

}
